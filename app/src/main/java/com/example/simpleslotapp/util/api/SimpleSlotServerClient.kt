package com.example.simpleslotapp.util.api

import com.example.simpleslotapp.data.SimpleSlotSplashResponse
import com.example.simpleslotapp.data.static.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface SimpleSlotServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<SimpleSlotSplashResponse>

    companion object {
        fun create() : SimpleSlotServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(SimpleSlotServerClient::class.java)
        }
    }
}