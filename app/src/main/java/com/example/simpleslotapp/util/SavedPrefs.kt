package com.example.simpleslotapp.util

import android.content.Context
import java.util.UUID

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveMoneyAmount(context: Context, amount: Int) = context
            .getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE)
            .edit()
            .putInt("money", amount)
            .apply()

        fun getMoneyAmount(context: Context): Int = context
            .getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE)
            .getInt("money", 5000)

    }
}