package com.example.simpleslotapp.ui.composables

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import com.example.simpleslotapp.data.static.Assets

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun Slot(modifier: Modifier = Modifier, imageUrl: String) {
    var lastIcon by remember { mutableStateOf(Assets.bonusBitmaps[imageUrl]) }
    var currentIcon by remember { mutableStateOf(Assets.bonusBitmaps[imageUrl]) }
    var currentIconUrl by remember { mutableStateOf(imageUrl) }

    var key by remember { mutableStateOf(0) }
    if(imageUrl != currentIconUrl) {
        key++
        lastIcon = currentIcon
        currentIcon = Assets.bonusBitmaps[imageUrl]
        currentIconUrl = imageUrl
    }

    BoxWithConstraints(modifier = modifier, contentAlignment = Alignment.Center) {
        AnimatedContent(targetState = key.toString(), label = "slot",
            transitionSpec = {
                slideInVertically( tween(500, delayMillis = 0)) { -it } with slideOutVertically(tween(500, delayMillis = 0)) { it }
            }) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                if(it == key.toString()) currentIcon?.let { it1 -> Image(it1, contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Fit) }
                else lastIcon?.let { it1 -> Image(it1, contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Fit) }
            }
        }
    }
}