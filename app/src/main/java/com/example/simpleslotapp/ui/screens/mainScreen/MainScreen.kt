package com.example.simpleslotapp.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.simpleslotapp.R
import com.example.simpleslotapp.data.static.UrlBack
import com.example.simpleslotapp.ui.composables.PlayButton
import com.example.simpleslotapp.ui.composables.Slot
import com.example.simpleslotapp.ui.composables.WinPopup
import com.example.simpleslotapp.ui.theme.Blue
import com.example.simpleslotapp.ui.theme.Gold
import com.example.simpleslotapp.ui.theme.Red

@Composable
fun MainScreen() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val moneyAmount by viewModel.money.collectAsState()
    val firstSlotImageUrl by viewModel.firstSlotImageUrl.collectAsState()
    val secondSlotImageUrl by viewModel.secondSlotImageUrl.collectAsState()
    val thirdSlotImageUrl by viewModel.thirdSlotImageUrl.collectAsState()
    val runButtonActive by viewModel.runButtonActive.collectAsState()
    val winPopup by viewModel.winPopup.collectAsState()

    Image(rememberAsyncImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(Blue)
            .padding(horizontal = 10.dp), contentAlignment = Alignment.Center) {
            Text(moneyAmount.toString(), fontSize = 30.sp, textAlign = TextAlign.Center, color = Gold)
            IconButton(onClick = { viewModel.addMoney() },
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .size(23.dp)
                    .border(1.dp, Gold, RoundedCornerShape(5.dp))
                    .clip(RoundedCornerShape(5.dp))) {
                Icon(painter = painterResource(id = R.drawable.round_add_24),
                    contentDescription = stringResource(id = R.string.desc_add_money),
                    modifier = Modifier.size(20.dp),
                    tint = Gold)
            }
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .padding(10.dp), contentAlignment = Alignment.Center) {
            Row(modifier = Modifier
                .fillMaxWidth()
                .background(Color.White, RoundedCornerShape(10.dp))
                .border(5.dp, Red, RoundedCornerShape(10.dp))
                .padding(15.dp), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                Slot(modifier = Modifier
                    .weight(1.0f, true)
                    .aspectRatio(1.0f), imageUrl = firstSlotImageUrl.imageUrl)
                Slot(modifier = Modifier
                    .weight(1.0f, true)
                    .aspectRatio(1.0f), imageUrl = secondSlotImageUrl.imageUrl)
                Slot(modifier = Modifier
                    .weight(1.0f, true)
                    .aspectRatio(1.0f), imageUrl = thirdSlotImageUrl.imageUrl)
            }
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .weight(0.5f, true)
            .padding(10.dp), contentAlignment = Alignment.Center) {
            PlayButton(modifier = Modifier.fillMaxWidth(0.5f), enabled = runButtonActive) {
                viewModel.run()
            }
        }
    }

    if(winPopup) WinPopup {
        viewModel.closePopup()
    }

}