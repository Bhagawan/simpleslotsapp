package com.example.simpleslotapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.simpleslotapp.R

@Preview
@Composable
fun PlayButton(modifier: Modifier = Modifier, enabled: Boolean = true, onPress: () -> Unit = {}) {
    val activeBrush = Brush.verticalGradient(
        0.0f to Color(0.62f, 0.839f, 0.373f, 1.0f),
        0.2f to Color(0.388f, 0.522f, 0.243f, 1.0f),
        0.4f to Color(0.227f, 0.306f, 0.141f, 1.0f),
        1.0f to Color(0.294f, 0.4f, 0.18f, 1.0f)
    )
    val inactiveBrush = Brush.verticalGradient(
        0.0f to Color(0.494f, 0.494f, 0.494f, 1.0f),
        0.2f to Color(0.294f, 0.294f, 0.294f, 1.0f),
        0.4f to Color(0.227f, 0.227f, 0.227f, 1.0f),
        1.0f to Color(0.388f, 0.388f, 0.388f, 1.0f)
    )
    TextButton(onClick = { onPress() }, enabled = enabled, modifier = modifier
        .clip(RoundedCornerShape(20.dp))
        .background(brush = if (enabled) activeBrush else inactiveBrush, shape = RoundedCornerShape(20.dp), 1.0f)
        .padding(10.dp)
    ) {
        Text(stringResource(id = R.string.btn_run), fontSize = 30.sp, textAlign = TextAlign.Center, color = Color.Black)
    }
}