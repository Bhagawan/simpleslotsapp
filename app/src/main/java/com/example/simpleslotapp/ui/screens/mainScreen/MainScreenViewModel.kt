package com.example.simpleslotapp.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.simpleslotapp.data.Bonuses
import com.example.simpleslotapp.data.static.AppData
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainScreenViewModel: ViewModel() {
    private val _money = MutableStateFlow(AppData.money)
    val money = _money.asStateFlow()

    private val _firstSlotImageUrl = MutableStateFlow(Bonuses.values().random())
    val firstSlotImageUrl = _firstSlotImageUrl.asStateFlow()

    private val _secondSlotImageUrl = MutableStateFlow(Bonuses.values().random())
    val secondSlotImageUrl = _secondSlotImageUrl.asStateFlow()

    private val _thirdSlotImageUrl = MutableStateFlow(Bonuses.values().random())
    val thirdSlotImageUrl = _thirdSlotImageUrl.asStateFlow()

    private val _runButtonActive = MutableStateFlow(true)
    val runButtonActive = _runButtonActive.asStateFlow()

    private val _winPopup = MutableStateFlow(false)
    val winPopup = _winPopup.asStateFlow()

    fun run() {
        if(AppData.money >= 100) {
            viewModelScope.launch {
                AppData.money -= 100
                _money.emit(AppData.money)
                _runButtonActive.emit(false)
                var d = 0
                while (d < 5000) {
                    val bonuses = Bonuses.values().toList()
                    _firstSlotImageUrl.emit(bonuses.minus(firstSlotImageUrl.value).random())
                    delay(100)
                    _secondSlotImageUrl.emit(bonuses.minus(secondSlotImageUrl.value).random())
                    delay(100)
                    _thirdSlotImageUrl.emit(bonuses.minus(thirdSlotImageUrl.value).random())
                    delay(400)
                    d+=600
                }

                if(firstSlotImageUrl.value == secondSlotImageUrl.value && secondSlotImageUrl.value == thirdSlotImageUrl.value) {
                    _winPopup.emit(true)
                    AppData.money += 1000
                    _money.emit(AppData.money)
                }
                if(money.value >= 100) _runButtonActive.emit(true)
            }
        }
    }

    fun closePopup() {
        _winPopup.tryEmit(false)
    }

    fun addMoney() {
        AppData.money += 1000
        _money.tryEmit(AppData.money)
    }
}