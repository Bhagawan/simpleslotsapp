package com.example.simpleslotapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    MAIN_SCREEN("main")
}