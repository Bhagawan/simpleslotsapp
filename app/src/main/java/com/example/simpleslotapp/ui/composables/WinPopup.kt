package com.example.simpleslotapp.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.scaleIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import coil.compose.rememberAsyncImagePainter
import com.example.simpleslotapp.data.static.UrlJackpot
import com.example.simpleslotapp.ui.theme.Transparent_black_2
import kotlinx.coroutines.delay

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun WinPopup(onClose: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(Transparent_black_2), contentAlignment = Alignment.Center) {
        AnimatedVisibility(visible = true,
            enter = scaleIn(spring(dampingRatio = Spring.DampingRatioMediumBouncy))
        ) {
            Image(rememberAsyncImagePainter(UrlJackpot), contentDescription = null, contentScale = ContentScale.FillWidth, modifier = Modifier.fillMaxWidth(0.7f))
        }
    }
    LaunchedEffect(key1 = "closeTimer") {
        delay(1000)
        onClose()
    }
}