package com.example.simpleslotapp.data

enum class AssetsState {
    LOADING, ERROR, LOADED
}