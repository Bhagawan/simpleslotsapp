package com.example.simpleslotapp.data

import androidx.annotation.Keep

@Keep
data class SimpleSlotSplashResponse(val url : String)