package com.example.simpleslotapp.data.static

const val UrlSplash = "SimpleSlotApp/splash.php"
const val UrlLogo = "http://195.201.125.8/SimpleSlotApp/logo.png"
const val UrlBack = "http://195.201.125.8/SimpleSlotApp/back.png"

const val UrlBonusStar = "http://195.201.125.8/SimpleSlotApp/assets/star.png"
const val UrlBonusSeven = "http://195.201.125.8/SimpleSlotApp/assets/seven.png"
const val UrlBonusBook = "http://195.201.125.8/SimpleSlotApp/assets/book.png"
const val UrlBonusCherry = "http://195.201.125.8/SimpleSlotApp/assets/cherry.png"
const val UrlBonusApple = "http://195.201.125.8/SimpleSlotApp/assets/apple.png"
const val UrlJackpot = "http://195.201.125.8/SimpleSlotApp/assets/jackpot.png"