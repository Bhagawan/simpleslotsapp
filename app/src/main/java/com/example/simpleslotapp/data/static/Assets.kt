package com.example.simpleslotapp.data.static

import android.content.Context
import android.graphics.Bitmap
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.core.graphics.drawable.toBitmap
import coil.ImageLoader
import coil.request.ErrorResult
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.simpleslotapp.data.AssetsState
import com.example.simpleslotapp.data.Bonuses
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

object Assets {
    private val downloadScope = CoroutineScope(EmptyCoroutineContext)

    val bonusBitmaps = mutableMapOf<String, ImageBitmap>()

    private val _assetsState = MutableStateFlow(AssetsState.LOADING)
    val assetsState = _assetsState.asStateFlow()

    fun loadAssets(context: Context) {

        val bonuses = Bonuses.values()
        for(bonus in bonuses) {
            downloadImage(context, bonus.imageUrl,
                onSuccess =  {
                    bonusBitmaps[bonus.imageUrl] = it.asImageBitmap()
                    checkCompletion()
                },
                onError =  {
                    _assetsState.tryEmit(AssetsState.ERROR)
                })
        }
    }


    private fun downloadImage(context: Context,
                               url: String,
                               onSuccess: (bitmap: Bitmap) -> Unit,
                               onError: (error: Throwable) -> Unit) {
        val loader = ImageLoader(context)
        val request = ImageRequest.Builder(context)
                .data(url)
                .size(200,400)
                .allowHardware(false)
                .listener(object: ImageRequest.Listener {
                    override fun onError(request: ImageRequest, result: ErrorResult) {
                        onError(result.throwable)
                        super.onError(request, result)
                    }

                    override fun onSuccess(request: ImageRequest, result: SuccessResult) {
                        onSuccess(result.drawable.toBitmap())
                    }
                })
                .build()
        downloadScope.launch {
            loader.execute(request)
        }
    }

    private fun checkCompletion() {
        val bonuses = Bonuses.values()
        var loaded = true
        for(bonus in bonuses) {
            if(!bonusBitmaps.contains(bonus.imageUrl)) {
                loaded = false
                break
            }
        }
        if(loaded) {
            MainScope().launch {
                _assetsState.emit(AssetsState.LOADED)
            }
        }
    }
}