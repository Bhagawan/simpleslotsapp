package com.example.simpleslotapp.data

import com.example.simpleslotapp.data.static.UrlBonusApple
import com.example.simpleslotapp.data.static.UrlBonusBook
import com.example.simpleslotapp.data.static.UrlBonusCherry
import com.example.simpleslotapp.data.static.UrlBonusSeven
import com.example.simpleslotapp.data.static.UrlBonusStar

enum class Bonuses(val imageUrl: String) {
    STAR(UrlBonusStar),
    SEVEN(UrlBonusSeven),
    BOOK(UrlBonusBook),
    CHERRY(UrlBonusCherry),
    APPLE(UrlBonusApple)
}